<?php
/**
 * @file
 * Main FlexiCache API.
 */

/**
 * Fetches data from the cache, optionally with a MongoDB query.
 */
function flexicache_get($cid_or_conditions, $table = 'cache') {
  return cache_get($cid_or_conditions, $table);
}

/**
 * Sets data in the cache, with additional meta data.
 */
function flexicache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL, $meta = array()) {
  return cache_set($cid, $data, $table, $expire, $headers, $meta);
}

/**
 * Clears data in the cache, optionally querying against meta data.
 */
function flexicache_clear_all($cid_or_conditions = NULL, $table = NULL, $wildcard = FALSE) {
  return cache_clear_all($cid_or_conditions, $table, $wildcard);
}

/**
 * Prepare meta properties for storing or querying in MongoDB.
 *
 * MongoDB is very type-sensitive, in contrast to MySQL. Numbers stored in
 * MongoDB need to always be queried and stored as actual numbers, or queries
 * will fail. This function casts numeric IDs to floats for that purpose.
 *
 * We also convert from single values to arrays and back. This is mostly a
 * convenience feature, because FlexiCache entity IDs are always stored and
 * queried as arrays in MongoDB.
 *
 * @param $meta
 *   Array of metadata to process.
 * @param $is_query
 *   Whether or not to treat the array as Mongo conditions. Defaults to FALSE.
 *
 * @return
 *   Standardized metadata array.
 */
function _flexicache_prepare_meta($meta, $is_query = FALSE) {
  foreach ($meta as &$value) {
    if (!is_array($value)) {
      $value = array($value);
    }
    foreach ($value as &$id) {
      if (is_numeric($id)) {
        $id = (float) $id;
      }
    }
    if ($is_query && count($value) == 1 && is_numeric(reset(array_keys($value)))) {
      $value = reset($value);
    }
  }

  return $meta;
}

/**
 * Merges two meta arrays together.
 *
 * @param $meta
 *   Array of metadata.
 * @param $more
 *   Another array of metadata to merge into $meta.
 *
 * @return
 *   Merged metadata array.
 */
function _flexicache_merge_meta($meta, $more) {
  $meta = _flexicache_prepare_meta($meta);
  $more = _flexicache_prepare_meta($more);
  $meta = array_merge_recursive($meta, $more);
  $meta = array_map('array_unique', $meta);

  return $meta;
}

/**
 * Helps in filling in the correct $expire and $meta and sets the block cache.
 */
function _flexicache_block_set($cid, $block, $data) {
  global $user;

  $lifetime = (int) variable_get('flexicache_block_lifetime', CACHE_TEMPORARY);
  if ($lifetime !== CACHE_TEMPORARY && $lifetime !== CACHE_PERMANENT) {
    $expire = time() + $lifetime;
  }
  else {
    $expire = $lifetime;
  }

  $meta = array();

  if ($block->cache & BLOCK_CACHE_PER_ROLE) {
    $meta['roles'] = array_keys($user->roles);
  }
  elseif ($block->cache & BLOCK_CACHE_PER_USER) {
    $meta['users'] = $user->uid;
  }

  if ($block->cache & BLOCK_CACHE_PER_PAGE) {
    $meta['paths'] = $_GET['q'];
  }

  // Allow extra metadata to be added at render time by hook_block().
  $custom_meta = isset($data['flexicache']) ? $data['flexicache'] : array();
  $meta = _flexicache_merge_meta($meta, $custom_meta);

  cache_set($cid, $data, 'cache_block', $expire, NULL, $meta);
}
