Superseded by Cache Tags project. The code here only acts as a
backwards-compatibility layer.

See http://drupal.org/project/cachetags for new development.