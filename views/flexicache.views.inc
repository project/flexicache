<?php
/**
 * @file
 * Views integration for FlexiCache.
 */

/**
 * Implementation of hook_views_plugins().
 */
function flexicache_views_plugins() {
  $plugins = array(
    'cache' => array(
      'flexicache' => array(
        'title' => t('FlexiCache'),
        'help' => t('Time-based cache with FlexiCache support.'),
        'handler' => 'flexicache_views_plugin_cache',
        'uses options' => TRUE,
        'parent' => 'time',
        'path' => drupal_get_path('module', 'flexicache') .'/views',
      ),
    ),
  );
  return $plugins;
}
