<?php

/**
 * FlexiCache plugin for Views.
 */
class flexicache_views_plugin_cache extends views_plugin_cache_time {
  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return t('FlexiCache');
  }

  /**
   * Save data to the cache.
   *
   * A plugin should override this to provide specialized caching behavior.
   */
  function cache_set($type) {
    switch ($type) {
      case 'query':
        // Not supported currently, but this is certainly where we'd put it.
        break;
      case 'results':
        $data = array(
          'result' => $this->view->result,
          'total_rows' => $this->view->total_rows,
          'current_page' => $this->view->get_current_page(),
        );
        flexicache_set($this->get_results_key(), $data, $this->table, $this->cache_set_expire($type), NULL, $this->get_cache_meta());
        break;
      case 'output':
        $this->gather_headers();
        $this->storage['output'] = $this->view->display_handler->output;
        flexicache_set($this->get_output_key(), $this->storage, $this->table, $this->cache_set_expire($type), NULL, $this->get_cache_meta());
        break;
    }
  }

  /**
   * Retrieve data from the cache.
   *
   * A plugin should override this to provide specialized caching behavior.
   */
  function cache_get($type) {
    $cutoff = $this->cache_expire($type);
    switch ($type) {
      case 'query':
        // Not supported currently, but this is certainly where we'd put it.
        return FALSE;
      case 'results':
        // Values to set: $view->result, $view->total_rows, $view->execute_time,
        // $view->current_page.
        if ($cache = flexicache_get($this->get_results_key(), $this->table)) {
          if (!$cutoff || $cache->created > $cutoff) {
            $this->view->result = $cache->data['result'];
            $this->view->total_rows = $cache->data['total_rows'];
            $this->view->set_current_page = $cache->data['current_page'];
            $this->view->execute_time = 0;
            return TRUE;
          }
        }
        return FALSE;
      case 'output':
        if ($cache = cache_get($this->get_output_key(), $this->table)) {
          if (!$cutoff || $cache->created > $cutoff) {
            $this->storage = $cache->data;
            $this->view->display_handler->output = $cache->data['output'];
            $this->restore_headers();
            return TRUE;
          }
        }
        return FALSE;
    }
  }

  /**
   * Clear out cached data for a view.
   *
   * We're just going to nuke anything related to the view, regardless of display,
   * to be sure that we catch everything. Maybe that's a bad idea.
   */
  function cache_flush() {
    flexicache_clear_all($this->view->name . ':', $this->table, TRUE);
  }

  /**
   * Get the view's entity IDs to tag the cache entry with.
   */
  function get_cache_meta() {
    $ids = array();
    $type = $this->view->base_table;
    switch ($this->view->base_table) {
      case 'node':
        $type = 'nodes';
        break;
    }
    foreach ($this->view->result as $row) {
      $ids[] = $row->{$this->view->base_field};
    }
    return array($type => $ids);
  }
}
